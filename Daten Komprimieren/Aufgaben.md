1.  Baumstruckturen sind in der Informatik sehr verbreitet. Das File-System von Windows ist ein gutes Beispiel dafür.
    Bei einem Binärem Baum hat ein Knoten maximal zwei Kinder, ansonsten kann die Anzahl beliebig sein.

2. 
![Alt text](image.png)

3.  Wenn es sich nicht nur um ein schwarz-weiss Bild handelt, dann muss man mehr Informationen mitgeben. Also auch   
    die  Farben. 80 Bit wären als Bandbreite sinnvoll damit da dies die Maximal mögliche Längge ist. 

4. ![Alt text](image-1.png)

8.  Es macht sinn Daten zu Komprimieren, wenn man sie speichern oder verschicken will. Denn so spaart man   
    Speicherplatz und das Verschicken geht schneller. 
    Würde man einen Brief oder Code verlustbehaftet komprimieren, würden Informationen verloren ghen. Dadurch kann es passieren, dass man den Brief nicht mehr verstehen kann oder, dass der Code nicht mehr funktioniert. 